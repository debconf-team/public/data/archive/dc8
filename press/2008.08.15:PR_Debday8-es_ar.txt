﻿15 de Agosto de 2008

PRESS RELEASE
COMUNICADO DE PRENSA

Buenos Aires se prepara para la conferencia de software libre DebianDay 
=======================================================================

* Expertos de todo el mundo responderán las preguntas sobre software open-source
* 14 presentaciones en ingles y en español
* La entrada es libre y gratuita

Buenos Aires, Argentina, 15 de Agosto de 2008 - El Proyecto Debian, el equipo
detrás del sistema operativo Debian GNU/Linux, invita a los miembros del publico
en la ciudad de Buenos Aires, Argentina. La conferencia comienza alas 10:00 en 
punto y esta estipulado su finalización para las 20:00 horas.

Debian Day es un evento gratuito, el evento provee un clima coloquial para que
los individuos que estén interesados en software libre y open-source se reúnan
y conozcan a las personas detrás del proyecto y puedan obtener consejos directamente
de los profesionales.

Los visitantes tendrán la oportunidad de presenciar 14 ponencias en dos
salas, con temáticas que incluyen administración de voluntarios, seguridad
libertad, uso de Software Libre en Latinoamerica y secretos de Debian.


Mas información, así como también el programa de la conferencia, esta disponible en:
  http://debianday.org


"El evento gratuito esta organizado completamente por voluntarios," dice Nicolás
Secreto, un entusiasta usuario de Debian de 9 de julio, Provincia de Buenos Aires,
que esta comenzando a participar activamente en el proyecto. "El resultado es
una atmósfera amigable y personal en donde es entretenido conocer a las personas
detrás del proyecto y encontrar respuestas a las preguntas que uno siempre se hace."

"Estoy muy entusiasmada de albergar Debian Day en la hermosa ciudad de Buenos
Aires," comenta Margarita Manterola, organizadora de la conferencia. "Debian
Day es una excelente manera de introducir Debian y el Software Libre a la 
comunidad local."


Debian Day continua luego de DebConf8, la conferencia anual de Debian, que tiene
lugar en Mar del Plata, Argentina, del 10 al 17 de Agosto de 2008.

FIN

Debian es un sistema operativo libre basado en Linux. Mas de 3000 voluntarios de 
todas partes del mundo trabajan juntos para crear y mantener el software de Debian. 
Traducido a 27 idiomas, y soportando una enorme cantidad de tipos de computadoras, Debian 
se llama a si mismo "el sistema operativo universal".
 
DebConf es la conferencia anual de Debian. Mas información sobre DebConf esta disponible en: 
  http://debconf.org
 
PARA MAS INFORMACION POR FAVOR CONTACTAR
  
Equipo de prensa de DebConf
press@debconf.org
 
Español:                 Ingles:
Sebastian Montini        Martin Krafft
Tel: 0223 155 560020     Tel: +41 44 6350919
  
Miembros de la prensa son bienvenidos a asistir al evento. Estaremos contentos de organizar 
entrevistas y proveer con información adicional
