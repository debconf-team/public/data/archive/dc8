6 de agosto de 2008

COMUNICADO DE PRENSA

Mar del Plata sera el anfitrión de la 9na conferencia de Debian
===============================================================

  * 250 desarrolladores de 37 países se juntarán en el Hotel Dora, Mar del Plata, Argentina, 10-16 de Agosto de 2008
  * 100 charlas y talleres están programados
  * La asistencia es totalmente gratuita, se requiere registro al llegar
  * El Proyecto Debian cumple 15 años


Mar del Plata, Argentina, 6 de agosto de 2008 - El Proyecto Debian, el equipo detrás del Sistema Operativo libre Debian GNU/Linux, invita al publico y la prensa a su conferencia anual, DebConf8, que se llevara a cabo desde el 10 de Agosto al 16 de Agosto en el Hotel Dora, en Mar del Plata, Argentina.

Mas de 100 charlas han sido programadas con disertantes de todas partes del mundo, como por ejemplo el líder del proyecto Debian, Steve McIntyre y el jefe de tecnologías Linux y open source de Hewlett Packard, Bdale Garbee. Los temas incluyen la organización del equipo global, internacionalización, desarrollos y políticas internas, virtualización, y administración de sistemas y redes. En los talleres, los asistentes podrán adquirir experiencia real en varios ámbitos. El desarrollo y las lluvias de ideas tendrán lugar en los dos "Hack Labs", y además también habrá otro tipo de eventos, como una fiesta con quesos y vinos de todo el mundo que permite una amplia posibilidad de socializar.

El listado completo de la conferencia esta disponible en:
  https://penta.debconf.org/dc8_schedule/

Las celebraciones tendrán lugar el sábado 16 de agosto, cuando el Proyecto Debian cumpla 15 años de antigüedad. Fundado por Ian Murdock en 1993 para ser un proyecto libre de la comunidad, el proyecto ha crecido para convertirse en uno de los mas grandes e influyentes proyectos de código abierto.

Jörg Jaspert, uno de los organizadores de la conferencia: "De todas las conferencias de Debian a las que he asistido, esta es la mejor preparada. ¡Gracias al equipo local por todo su trabajo!". El equipo local de organizadores - 40 voluntarios de todas partes del país - ha dedicado casi un año entero de su tiempo para hacer que este evento salga adelante.

La entrada a la conferencia es gratis, pero se requiere que los visitantes se registren en al llegar. Mas información esta disponible en el sitio web de la conferencia: http://debconf8.debconf.org

El idioma de las presentaciones es en inglés, aunque hay muchos hispanoparlantes disponibles para ayudar y responder cualquier pregunta.

Todos los eventos son grabados y transmitidos por internet. Más información puede ser encontrada en: http://debconf8.debconf.org/video

El miércoles, 13 de agosto, no habrá charlas ya que los asistentes de la conferencia tendrán un día libre para experimentar la cultura local.

A continuación de DebConf 8, tendrá lugar el Debian Day el 18 de agosto en Buenos Aires. La entrada al Debian Day es gratis. El evento provee una atmósfera amigable y personal para los individuos que estén interesados en el software libre y de código abierto para conocer la  gente detrás del proyecto.

FIN


Debian es un sistema operativo libre basado en Linux. Mas de 3000 voluntarios de todas partes del mundo trabajan juntos para crear y mantener el software de Debian. Traducido a 27 idiomas, y soportando una enorme cantidad de tipos de computadoras, Debian se llama a si mismo "el sistema operativo universal".

DebConf es la conferencia anual de Debian. Mas información sobre DebConf esta disponible en: http://debconf.org


PARA MAS INFORMACION POR FAVOR CONTACTAR

Equipo de prensa de DebConf
press@debconf.org

Español:                 Ingles:
Sebastian Montini        Martin Krafft
Tel: 0223 155 560020     Tel: +41 44 6350919

Miembros de la prensa son bienvenidos a asistir al evento. Estaremos contentos de organizar entrevistas y proveer con información adicional
