\contentsline {section}{Preface}{i}{section*.1}
\contentsline {chapter}{Contents}{ii}{section*.2}
\contentsline {chapter}{\chapternumberline {1}Knowledge, Power and free Beer}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Free Beer}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Hard and soft Ware}{2}{subsection.1.1.1}
\contentsline {subsubsection}{Hard Ware}{2}{section*.3}
\contentsline {subsubsection}{Soft Ware}{3}{section*.4}
\contentsline {subsubsection}{Fresh fish}{4}{section*.5}
\contentsline {subsection}{\numberline {1.1.2}Patent recipes}{4}{subsection.1.1.2}
\contentsline {subsubsection}{The progress}{4}{section*.6}
\contentsline {subsubsection}{EU: Council versus Parliament}{5}{section*.7}
\contentsline {subsection}{\numberline {1.1.3}Free of charge}{6}{subsection.1.1.3}
\contentsline {subsubsection}{Soft ware is not for sale}{6}{section*.8}
\contentsline {subsubsection}{The catch: Free $\not =$ free of charge}{6}{section*.9}
\contentsline {section}{\numberline {1.2}Knowledge}{7}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Alternatives}{7}{subsection.1.2.1}
\contentsline {subsubsection}{WikiPedia}{7}{section*.10}
\contentsline {subsubsection}{WikiPedia is taken honestly}{7}{section*.11}
\contentsline {subsection}{\numberline {1.2.2}Free Software in general}{9}{subsection.1.2.2}
\contentsline {subsubsection}{As You Like it}{9}{section*.12}
\contentsline {subsubsection}{Get the facts}{11}{section*.13}
\contentsline {section}{\numberline {1.3}Power}{11}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Distribution}{11}{subsection.1.3.1}
\contentsline {subsubsection}{Linux from scratch}{11}{section*.14}
\contentsline {subsubsection}{Distributions on the market}{12}{section*.15}
\contentsline {subsubsection}{The missing link}{14}{section*.16}
\contentsline {subsection}{\numberline {1.3.2}Debian - a distribution builded by a community}{14}{subsection.1.3.2}
\contentsline {subsubsection}{Maintainers are experts}{14}{section*.17}
\contentsline {subsubsection}{Special applications}{15}{section*.18}
\contentsline {subsubsection}{Lost in the jungle of applications}{15}{section*.19}
\contentsline {subsubsection}{Custom Debian Distributions}{16}{section*.20}
\contentsline {chapter}{\chapternumberline {2}Solving Package Dependencies}{18}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}The Past: EDOS}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Formalization of Inter-Package Relations}{20}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Results, Tools, and Applications}{24}{subsection.2.2.2}
\contentsline {subsubsection}{Result: Installability is NP-complete}{24}{section*.22}
\contentsline {subsubsection}{Tools: edos-debcheck, pkglab and ceve}{25}{section*.23}
\contentsline {subsubsection}{Application: Finding Uninstallable Packages in Debian}{26}{section*.24}
\contentsline {subsubsection}{Application: Debian Weather}{27}{section*.25}
\contentsline {subsubsection}{Application: Finding File Conflicts in Debian}{31}{section*.26}
\contentsline {section}{\numberline {2.3}Present and Future: Mancoosi}{33}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}An Overview of the Mancoosi Project}{33}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Dependency solving}{35}{subsection.2.3.2}
\contentsline {subsubsection}{Completeness}{35}{section*.27}
\contentsline {subsubsection}{Optimality}{36}{section*.28}
\contentsline {subsubsection}{Efficiency}{37}{section*.29}
\contentsline {subsection}{\numberline {2.3.3}A solver competition}{37}{subsection.2.3.3}
\contentsline {paragraph}{Upgrade problem database}{38}{section*.30}
\contentsline {paragraph}{Types of competitions}{39}{section*.31}
\contentsline {paragraph}{Upgrade Description Formats}{39}{section*.32}
\contentsline {subsection}{\numberline {2.3.4}Debian and Mancoosi}{41}{subsection.2.3.4}
\contentsline {chapter}{\chapternumberline {3}Best practises in team package maintenance}{44}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{44}{section.3.1}
\contentsline {section}{\numberline {3.2}Questions}{45}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Basics}{45}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Work flow}{45}{subsection.3.2.2}
\contentsline {paragraph}{Communication}{45}{section*.34}
\contentsline {paragraph}{Members}{45}{section*.35}
\contentsline {paragraph}{Review/uploads/sponsoring}{45}{section*.36}
\contentsline {paragraph}{Infrastructure}{45}{section*.37}
\contentsline {paragraph}{Policy/rules}{45}{section*.38}
\contentsline {paragraph}{Tools}{45}{section*.39}
\contentsline {subsection}{\numberline {3.2.3}Experiences}{46}{subsection.3.2.3}
\contentsline {paragraph}{Best practises/successes}{46}{section*.40}
\contentsline {paragraph}{Challenges/failures}{46}{section*.41}
\contentsline {subsection}{\numberline {3.2.4}Other teams/Debian}{46}{subsection.3.2.4}
\contentsline {chapter}{\chapternumberline {4}Custom Debian Distributions}{48}{chapter.4}
\contentsline {section}{\numberline {4.1}Symbiosis between experts and developers}{49}{section.4.1}
\contentsline {section}{\numberline {4.2}Attracting people by providing interesting technical base}{50}{section.4.2}
\contentsline {section}{\numberline {4.3}CDD is more than packaging specific software}{51}{section.4.3}
\contentsline {section}{\numberline {4.4}Techniques}{52}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Building metapackages and tasksel descriptions}{52}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Web pages based on common scripts}{52}{subsection.4.4.2}
\contentsline {chapter}{\chapternumberline {5}The Debian Videoteam --- Behind the Scenes}{54}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{54}{section.5.1}
\contentsline {paragraph}{People}{54}{section*.42}
\contentsline {paragraph}{Hardware}{54}{section*.43}
\contentsline {paragraph}{Software}{54}{section*.44}
\contentsline {paragraph}{TODO}{54}{section*.45}
\contentsline {paragraph}{Caveats}{55}{section*.46}
\contentsline {section}{\numberline {5.2}What the software does}{55}{section.5.2}
\contentsline {section}{\numberline {5.3}Live demo}{55}{section.5.3}
\contentsline {chapter}{\chapternumberline {6}DebConf9 Caceres}{58}{chapter.6}
\contentsline {section}{\numberline {6.1}Introduction}{58}{section.6.1}
\contentsline {section}{\numberline {6.2}Information Society Project in Extremadura}{59}{section.6.2}
\contentsline {section}{\numberline {6.3}gnuLinEx}{60}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Educational System}{60}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Public Administration}{61}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Health and Care System}{61}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}Network in Extremadura}{62}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Regional Intranet}{62}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Broadband Internet access for 100\% of population}{62}{subsection.6.4.2}
\contentsline {section}{\numberline {6.5}Why C\IeC {\'a}ceres?}{63}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Overview of C\IeC {\'a}ceres}{63}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}History}{63}{subsection.6.5.2}
\contentsline {subsection}{\numberline {6.5.3}Monuments}{64}{subsection.6.5.3}
\contentsline {subsection}{\numberline {6.5.4}Natural Parks and rural tourism}{65}{subsection.6.5.4}
\contentsline {chapter}{\chapternumberline {7}Abstracts}{66}{chapter.7}
\contentsline {section}{\numberline {7.1}Debian Webservices Development}{66}{section.7.1}
\contentsline {section}{\numberline {7.2}dak discussion / hacking session}{66}{section.7.2}
\contentsline {section}{\numberline {7.3}Emdebian update}{66}{section.7.3}
\contentsline {section}{\numberline {7.4}Virtualisation in Debian}{67}{section.7.4}
\contentsline {section}{\numberline {7.5}Debian on the Neo1973/Freerunner}{67}{section.7.5}
\contentsline {section}{\numberline {7.6}Debian Edu 100\% in main}{67}{section.7.6}
\contentsline {section}{\numberline {7.7}SPI BOF}{67}{section.7.7}
\contentsline {section}{\numberline {7.8}Managing 666 packages, or how to tame the beast}{67}{section.7.8}
\contentsline {section}{\numberline {7.9}dh\_make\_webapp: yeah right!}{68}{section.7.9}
\contentsline {section}{\numberline {7.10}Locating bugs to kill with SOAP}{68}{section.7.10}
\contentsline {section}{\numberline {7.11}Ruby packaging in Debian}{68}{section.7.11}
\contentsline {section}{\numberline {7.12}Organizing better in-person meetings}{68}{section.7.12}
\contentsline {section}{\numberline {7.13}Debian Derivers Roundtable}{69}{section.7.13}
\contentsline {section}{\numberline {7.14}Debian and LiMux}{69}{section.7.14}
\contentsline {section}{\numberline {7.15}Debian-Science}{69}{section.7.15}
\contentsline {section}{\numberline {7.16}Synfig - Animation in the free world}{69}{section.7.16}
\contentsline {section}{\numberline {7.17}Packaging with version control systems}{70}{section.7.17}
\contentsline {section}{\numberline {7.18}Debian and Ubuntu}{70}{section.7.18}
\contentsline {section}{\numberline {7.19}Herding Wild Cats}{70}{section.7.19}
\contentsline {section}{\numberline {7.20}LaTeX Beamer Debian Theme BOF}{70}{section.7.20}
\contentsline {section}{\numberline {7.21}Quality Assurance in lenny$+1$}{70}{section.7.21}
\contentsline {section}{\numberline {7.22}Healthy CDDs}{71}{section.7.22}
\contentsline {section}{\numberline {7.23}Debian-Med BOF}{71}{section.7.23}
\contentsline {section}{\numberline {7.24}Method diffusion in large volunteer projects}{71}{section.7.24}
\contentsline {section}{\numberline {7.25}Best practises in team-maintaining packages}{71}{section.7.25}
\contentsline {section}{\numberline {7.26}netconf}{72}{section.7.26}
\contentsline {section}{\numberline {7.27}Bringing closer Debian and Rails}{72}{section.7.27}
\contentsline {section}{\numberline {7.28}Internationalization in Debian}{72}{section.7.28}
\contentsline {section}{\numberline {7.29}Redesigning DEHS (a.k.a. changing the watch files atmosphere)}{73}{section.7.29}
\contentsline {section}{\numberline {7.30}Debian-HPC: making Debian "the" distro for clusters and supercomputers}{73}{section.7.30}
\contentsline {section}{\numberline {7.31}Bits from NMs and users}{73}{section.7.31}
\contentsline {section}{\numberline {7.32}Multi winner voting in Debian}{73}{section.7.32}
\contentsline {section}{\numberline {7.33}Debian technical policy update}{73}{section.7.33}
\contentsline {section}{\numberline {7.34}Debian Wiki}{74}{section.7.34}
\contentsline {section}{\numberline {7.35}Debian: casos de \IeC {\'e}xito en implementaciones empresariales}{74}{section.7.35}
\contentsline {section}{\numberline {7.36}Internacionalizaci\IeC {\'o}n en Debian}{74}{section.7.36}
\contentsline {section}{\numberline {7.37}Proyecto gnuLinEx}{75}{section.7.37}
\contentsline {section}{\numberline {7.38}\IeC {\textquestiondown }Qu\IeC {\'e} es el Software Libre?}{75}{section.7.38}
\contentsline {section}{\numberline {7.39}Hosting Caseros}{75}{section.7.39}
