[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Página principal"
  lang = "es"
%]
            <!--#include virtual="remaining.xhtml.es" -->
            <h2>Qué es DebConf?</h2>
            <h3>La conferencia de Debian</h3>
            <p>La conferencia de Debian es la reunión anual de desarrolladores
            de Debian, un evento lleno de discusiones, talleres y reuniones de
            programación, con un elevado contenido técnico. En esta
            oportunidad, será llevada a cabo en la ciudad de Mar del Plata, en
            Argentina, del 10 al 16 de agosto de 2008.</p>

            <p>Disertantes de todo el mundo se han dado cita para conferencias
            anteriores, que resultaron extremadamente útiles para desarrollar y
            mejorar componentes esenciales de Debian, como el instalador, o
            mejorar la internacionalización de Debian.</p>

            <h3>Debian Camp</h3>

            <p>Además, previo a la conferencia, algunos equipos de trabajo como
            los equipos del instalador de Debian o de Debian-Edu, tienen la
            posibilidad de reunirse para trabajar cara a cara en un ambiente
            sin distracciones. Sin embargo, hay que tener en cuenta que durante
            este período sólo las necesidades básicas como alojamiento y
            conectividad son provistas por la organización del evento. Los
            equipos interesados en participar deben contactar a los
            organizadores con anticipación.</p>

            <h3>Día de Debian</h3>

            <p>A la vez, los ejecutivos y personas de negocios interesados en
            descubrir los beneficios del Software Libre y los usuarios de
            Debian están invitados a participar del Día de Debian, un evento a
            puertas abiertas charlas de interés general. A diferencia de años
            anteriores, esta vez será realizado al finalizar la conferencia y
            en Buenos Aires, con el objetivo de sumar la mayor cantidad de
            público posible. Su proximidad con las Jornadas Regionales de
            Software Libre hará que la comunidad en pleno se dé cita.</p>

            <h3>Calendario</h3>
            <p>El calendario ha sido transladado a la página de <a
              href="dates.xhtml[% langext %]">[% t.leftmenu.dates %]</a>.</p>
