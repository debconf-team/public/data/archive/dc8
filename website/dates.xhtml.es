[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Fechas importantes"
  lang = "es"
%]
            <h2>Fechas importantes de DebConf8</h2>

            <p>¡Visite seguido esta página! Iremos cargando las fechas
            importantes que deba tener en cuenta.</p>

            <table class="fancy-table">
              <col class="date" />
              <col class="description" />
              <thead>
                <tr class="rowH">
                  <th>Fecha</th>
                  <th>Descripción</th>
                </tr>
              </thead>
              <thead>
                <tr class="rowH">
                  <th colspan="2">Junio de 2008.</th>
                </tr>
              </thead>
              <tbody>
                <tr class="rowA">
                  <td><strong>Domingo 15</strong></td>
                  <td><strong>Fecha límite de reconfirmación.</strong><br/>
                    ¡Si no reconfirma para esta fecha, pierde el beneficio de
                    tener comida y alojamiento pagos!<br />
                    Por favor, si reconfirma y luego decide no venir, avísenos
                    con antelación. Las personas que no avisen serán
                    penalizadas en la asignación de recursos para el próximo
                    año.
                  </td>
                </tr>
                <tr class="rowB">
                  <td><strong>Lunes 30</strong></td>
                  <td><strong>Fecha límite para presentación de
                      papers.</strong><br/>
                    Si no envía su paper a tiempo, su charla NO será incluída
                    en los proceedings de la conferencia.
                  </td>
                </tr>
              </tbody>
              <thead>
                <tr class="rowH">
                  <th colspan="2">Julio de 2008.</th>
                </tr>
              </thead>
              <thead>
                <tr class="rowH">
                  <th colspan="2">Agosto de 2008.</th>
                </tr>
              </thead>
              <tbody>
                <tr class="rowA">
                  <td>Sábado 2</td><td>Preparación</td></tr>
                <tr class="rowB">
                  <td>Domingo 3</td>
                  <td>
                    Primer día de DebCamp<br/>
                    Primer día de alojamiento y comida pagos (sólo para
                    personas con un plan de trabajo para DebCamp).
                  </td>
                </tr>
                <tr class="rowA">
                  <td>Sábado 9</td>
                  <td>
                    Último día de DebCamp (y día de llegada para DebConf)<br />
                    Primer día de alojamiento y comida pagos para personas
                    que asistan a DebConf.
                  </td>
                </tr>
                <tr class="rowB">
                  <td><strong>Domingo 10</strong></td><td><strong>Primer día de
                      las charlas de DebConf</strong></td></tr>
                <tr class="rowA">
                  <td>Miércoles 13</td><td>Day trip</td></tr>
                <tr class="rowB">
                  <td>Miércoles 13</td><td>Cena de la conferencia</td></tr>
                <tr class="rowA">
                  <td><strong>Sábado 16</strong></td>
                  <td>
                    <strong>Último día de charlas de DebConf</strong><br />
                    Último día de comida y alojamiento pagos. Debe abandonar el
                    hotel el 17 a la mañana!
                  </td>
                </tr>
                <tr class="rowB">
                  <td>Domingo 17</td><td>Desmantelamiento y desalojo del
                    hotel</td></tr>
                <tr class="rowA">
                  <td>Lunes 18</td><td>Día de Debian (en Buenos Aires)</td></tr>
              </tbody>
            </table>
