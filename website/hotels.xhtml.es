[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Los hoteles de DebConf"
  lang = "en"
%]
            <h2>Los hoteles de DebConf</h2>

            <h3>Hotel principal y sede de la conferencia: Hotel Dorá</h3>

            <p><span class="thumb"><a
                href="[% imgs %]/Dora_facade.jpg"
                ><img src=
                "[% imgs %]/preview_Dora_facade.jpg"
                alt="Fachada del Hotel Dorá" title="Fachada del Hotel Dorá"/></a>
              <br/>Fachada del Hotel Dorá</span>
            Este hotel, recientemente remodelado, ofrece un servicio de cuatro
            estrellas en pleno centro de la ciudad. Aunque fue construido en
            los años cincuenta, hoy ostenta un estilo actual con detalles de
            categoría. En la entrada, un gran lobby con cómodos sillones y un
            bar en el fondo dan la bienvenida.</p>

            <p>En el primer piso se encuentra el enorme salón comedor, donde se
            sirve el desayuno, almuerzo y cena. Las bebidas, salvo el agua de
            la canilla, serán abonadas por los participantes. En el mismo salón
            se encuentra un segundo bar. Al ser el salón tan grande, se
            aprovechará en parte como hacklab.</p>

            <p>Las disertaciones tendrán lugar en el séptimo piso, cuyo salón
            principal ofrece una imponente vista de la costa. Al lado del salón
            principal, se encuentra una sala secundaria de charlas y un foyer,
            donde se instalará un segundo hacklab.</p>

            <h4>Dirección y contacto</h4>
            <p><a href="http://www.hoteldora.com.ar/">Página web del Hotel
              Dorá</a> (requiere flash)<br />
            Buenos Aires 1841<br />
            (B7600EEC) Ciudad de Mar del Plata<br />
            Buenos Aires, Argentina<br />
            +54 223 491-0033<br />
            +54 800 222-7600</p>

            <h3>Hotel secundario: Hotel Ástor</h3>

            <p>Como la capacidad del primer hotel no es suficiente para todos
            los huéspedes de DebConf, un segundo hotel de similares
            características será utilizado para hospedaje únicamente. El Ástor
            es también un hotel de cuatro estrellas, con confortables
            habitaciones y un personal dispuesto a dar la mejor atención.</p>

            <p>Este hotel fue construido en la misma época que el Dorá, y
            mantiene su estilo más clásico y suntuoso. Está ubicado a menos de
            tres cuadras que se recorren en un par de minutos y dan la
            oportunidad de dar un paseo por la Peatonal San Martín, un típico
            paseo comercial de la ciudad.</p>

            <p>Al llegar, mucha madera y cómodos sillones dan la bienvenida, un
            pequeño bar invita a disfrutar un trago. El desayuno se sirve en
            el primer piso, en el "Salón de los espejos".</p>

            <h4>Dirección y contacto</h4>
            <p><a href="http://www.hotelastor.com.ar/">Sitio web del Hotel
              Ástor</a><br />
            Entre Ríos 1649<br />
            (B7600EDC) Ciudad de Mar del Plata<br />
            Buenos Aires, Argentina<br />
            +54 223 491-1616 to 19</p>
