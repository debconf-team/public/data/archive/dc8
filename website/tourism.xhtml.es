[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Información turística"
  lang = "es"
%]
            <h2>Información turística</h2>
            <p>Si planeás hacer turismo antes o después de DebConf, trataremos de
            darte alguna ayuda. Esta página está bastante incompleta en este momento,
			si querés que agreguemos información específica, contactate con
			algún miembro del equipo local.
			</p>

            <ul>
              <li><a href="http://wikitravel.org/es/Argentina">Página de
                Wikitravel sobre Argentina</a></li>
              <li><a
                href="http://www.argentina.gov.ar/argentina/portal/paginas.dhtml?pagina=73"
                >Información turística</a> provista por el gobierno</li>
              <li><a href="http://www.lonelyplanet.com/worldguide/argentina/"
                >Lonely Planet</a> siempre proveé buena información, sus guías
              impresas son recomendables. (No, no cobramos por esto :-))</li>
              <li><a href="http://www.argentinaturismo.com.ar/">Argentina
                Turismo</a>, portal privado.</li>
            </ul>

            <p>Ésta es también una buena oportunidad para viajar junto a otros
            Debianitas, ¡planeá con anticipación!</p>

            <h3>Buenos Aires</h3>
            <p>El gobierno de la ciudad de Buenos Aires ha armado unas guías
            turísticas muy interesantes, disponibles en varios idiomas, que se
            pueden bajar 
            <a
              href="http://www.bue.gov.ar/informacion/?menu_id=121&amp;info=guias"
              >acá</a>.</p>

            <h3>Salud</h3>
            <p>No suele ser necesario vacunarse antes de venir a Argentina,
            excepto que se visiten las provincias del norte (Misiones,
            Corrientes, Chaco, Formosa, Salta y Jujuy: esto incluye las
            Cataratas del Iguazú), donde hay riesgo de fiebre amarilla, por la
            epidemia que está sufriendo Paraguay. En este caso, ¡asegurate de
            vacunarte 10 días antes de venir!</p>

            <p>El agua de la canilla suele ser potable en las ciudades, ante la
            duda consultá con un local.</p>
