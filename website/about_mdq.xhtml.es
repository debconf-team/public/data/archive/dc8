[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Sobre MDQ"
  lang = "es"
%]
            <h2>Sobre MDQ</h2>
            <p><span class="thumb"><a
                href="http://www.mardelplata.gov.ar/img/turismo/fotos/Panor%E1mica%20Aerea.jpg"><img
                src="[% imgs %]/preview_Panoramica_Aerea.jpg"
                alt="MDQ desde el cielo" title="MDQ desde el cielo"/></a>
              <br/>MDQ desde el cielo</span>
            <span class="thumb"><a
                href="http://www.mardelplata.gov.ar/img/turismo/fotos/Cabo%20Corrientes.jpg"
                ><img src="[% imgs %]/preview_Cabo_Corrientes.jpg"
                title="Vista panorámica del Cabo Corrientes"
                alt="Vista panorámica del Cabo Corrientes"/></a>
              <br/>Vista panorámica del Cabo Corrientes</span>
            <span class="thumb"><a
                href="http://www.mdp.com.ar/img/webcam.jpg"><img
                src="http://www.mdp.com.ar/img/webcam.jpg"
                title="Webcam apuntando a la Plaza Colón"
                alt="Webcam apuntando a la Plaza Colón"/></a>
              <br/>Webcam apuntando a la Plaza Colón</span>
            <span class="thumb"><a
                href="http://www.mardelplata.gov.ar/img/turismo/fotos/Banquina%20de%20Pescadores.jpg"><img
                src="[% imgs %]/preview_Banquina_de_Pescadores.jpg"
                alt="El puerto" title="El puerto"/></a><br/>El puerto</span>
            Mar del Plata se encuentra aproximadamente a 400km al sur de Buenos
            Aires, Argentina. Durante los meses de verano, de Diciembre a
            Marzo, las playas están repletas de <a
              href="http://es.wikipedia.org/wiki/Porte%C3%B1o">Porteños</a> en
            vacaciones.</p>

            <p>Fuera de temporada veraniega, la ciudad se encuentra bastante
            menos poblada. A pesar de esto hay 700,000 residentes que viven
            allí durante todo el año. Es una muy extensa cuidad con un montón
            de cosas para hacer, y no solo durante los meses de verano.</p>

            <p>Se puede ver un Google map con algunos de los lugares más
            importantes de la ciudad  <a
              href="http://maps.google.com/maps/ms?msa=0&amp;msid=115031546281715831674.0004381c8c90f6fa8460b&amp;t=h">aquí</a></p>

            <p>El trasfondo social y lingüístico de la ciudad es la llamada 
            cultura <a
              href="http://es.wikipedia.org/wiki/Espa%C3%B1ol_rioplatense"
              title="Español rioplatense"><em>rioplatense</em></a>.</p>

            <h3>Cómo llegar... </h3>
            <p>El servicio de buses Manuel Tienda León está disponible
            desde el aeropuerto internacional de Ezeiza (EZE) por cerca de 90
            pesos por viaje. El viaje es de poco menos de 5 horas a través de
            las Pampas Argentinas. Esta opción se recomienda a los que llegan
            por vía aérea</p>

            <p>Cerca de diez diferentes compañías de buses de larga distancia
            con dos o mas frecuencias al día.</p>

            <p>Hay servicios diarios de tren desde Buenos Aires. De todos
            modos, los buses son mucho más rápidos y agradables</p>

            <h3>Cómo moverse... </h3>
            <p>Hay muchas líneas de bus urbano para moverse por la ciudad, con
            tarifas accesibles. Se puede encontrar un poco de información <a
              href="http://www.turismomardelplata.gov.ar/contenido/Transportes_mar_del_plata.asp?categoria=18&amp;destinos=-1"
              >aquí</a></p>

            <p>Los Taxis son muy económicos, cómodos y bastante honestos para
            trasladarse. Aunque Mar del Plata es perfecta para caminar.</p>

            <h3>Para ver... </h3>
            <p><span class="thumb"><a
                href="http://www.mardelplata.gov.ar/img/turismo/fotos/Escollera%20Norte.jpg"
                ><img src="[% imgs %]/preview_Escollera_Norte.jpg"
                alt="Escollera norte" title="Escollera norte"
                /></a><br/>Escollera norte</span>
            <span class="thumb"><a
                href="http://upload.wikimedia.org/wikipedia/commons/b/ba/Mar_del_Plata-Museo_Hist%C3%B3rico.jpg"
                title="Museo Histórico de la Ciudad"><img
                alt="Museo Histórico de la Ciudad"
                src="[% imgs %]/preview_Museo_Historico.jpg" /></a><br/>
              Museo Histórico de la Ciudad
            </span>
            Museos más importantes:</p>
            <ul>
              <li><b>Museo de arte moderno</b> <a
                href="http://www.welcomeargentina.com/paseos/museo_castagnino/index.html"
                ><em>Juan Carlos Castagnino</em></a>.</li>
              <li><b>Museo del hombre del puerto</b> <a
                href="http://www.welcomeargentina.com/paseos/museo_hombre_del_puerto/index.html"
                ><em>Cleto Ciocchini</em></a>.</li>
              <li><b>Museo de ciencias naturales</b> <a
                href="http://www.museosargentinos.org.ar/museos/museo.asp?codigo=79"
                ><em>Lorenzo Scaglia</em></a>, especializado en <a
                href="http://es.wikipedia.org/wiki/Paleontolog%C3%ADa"
                title="Paleontología">paleontología</a> de especies del <a
                href="http://es.wikipedia.org/wiki/Per%C3%ADodo_cuaternario"
                title="Período cuaternario">período cuaternario</a> de la 
              región.</li>

              <li><b><a
                href="http://www.welcomeargentina.com/paseos/museo_mar/index.html"
                >Museo del mar</a></b>, que contiene una de las colecciones más
              completas de <a
                href="http://es.wikipedia.org/wiki/Caracol"
                title="caracoles">caracoles</a> de mar del mundo. Interesante
              visita para una tarde lluviosa.</li>
              <li><a
                href="http://www.welcomeargentina.com/paseos/centro_cultural_victoria_ocampo/index.html"
                ><b>Villa Victoria</b></a>, una antigua casa de madera, que
              fue la residencia de la escritora <a
                href="http://en.wikipedia.org/wiki/Victoria_Ocampo"
                title="Victoria Ocampo">Victoria Ocampo</a>, hoy en día un 
              lugar para exposiciones de arte y música clásica.</li>
            </ul>
            <p>Más información sobre museos <a
              href="http://www.turismomardelplata.gov.ar/contenido/Museos_Mar_Del_Plata.asp">aquí</a>.
            También hay un a página sobre <a
              href="http://www.turismomardelplata.gov.ar/contenido/Arquitectura_Mar_Del_Plata.asp"
              >patrimonio arquitectónico</a>.</p>
            <p>Otros lugares de interés:</p>
            <ul>
              <li><b>"La Rambla"</b>, <b>"Las Recovas"</b>, la <b>"Plaza
                seca"</b> y la <b>"Plaza Colón"</b>.</li>

              <li><b>Los lobos de mar</b>. En la Rambla se encuentran las dos
              esculturas de lobos marinos, símbolos de la ciudad, obra del
              escultor José Fioravanti.</li>

              <li><b>Iglesia Catedral</b>: declarada <em>"Patrimonio Histórico
                Nacional"</em>. Fue inaugurada el 12 de febrero de 1905.</li>

              <li><b>Torreón del Monje</b>: inaugurado en 1904, es uno de los
              referentes obligados de la costa. Ubicado sobre la barranca de
              <b>Punta Piedras</b>, en su interior funcionan un restaurante y
              una confitería y lo convierten en un punto panorámico obligado
              por los turistas.</li>

              <li><b><a href="http://www.webmdp.com/paseolabanquina"
                  >Banquina de Pescadores</a></b> o <b>"muelle de pesca"</b>,
              ubicado en el Puerto. Este lugar típico alberga docenas de
              coloridas embarcaciones de pescadores. Estos barquitos
              generalmente salen a pescar muy temprano a la mañana y vuelven al
              atardecer. A la tarde hay remate de pescados, y es posible
              comprar pescado directamente de los barcos.</li>

              <li><b><a href="http://www.parquecamet.com.ar">Parque
                  Camet</a></b>, amplio parque municipal, arbolado, con
              confiterías, fogones, juegos, alquiler de caballos y rodados,
              etc.</li>

              <li><b>Barrio Los Troncos</b>: pintoresco barrio residencial,
              cuyo nombre es de 1938, cuando un acaudalado salteño hizo
              construir un chalé edificado con troncos de quebracho y lapacho.
              Con posterioridad se originó a su alrededor la edificación de
              casonas de similares características.</li>

              <li><b>Monumento a <a
                  href="http://es.wikipedia.org/wiki/Alfonsina_Storni"
                  title="Alfonsina Storni">Alfonsina Storni</a></b>: próximo al
              lugar donde la escritora Alfonsina Storni se internó al mar,
              suicidándose el 25 de octubre de 1938. Se trata de un sencillo
              monumento recordatorio, obra del escultor Luis Perlotti (1942).
              Frente al mar, la figura tallada en la piedra está acompañada por
              los versos del poema "Dolor", escrito por la artista en 1925. Una
              réplica de la obra se encuentra en el <em>Panteón de los
                Artistas</em> del <a
                href="http://es.wikipedia.org/wiki/Cementerio_de_Chacarita"
                >Cementerio de Chacarita</a>, en la <a
                href="http://es.wikipedia.org/wiki/Ciudad_de_Buenos_Aires"
                >ciudad de Buenos Aires</a>.</li>

              <li><b><a href="http://es.wikipedia.org/wiki/Faro_Punta_Mogotes"
                  >Faro Punta Mogotes</a></b>:
              construido en 1890. Se encuentra en una colina a 26 m. sobre el
              nivel del mar, así el faro alcanza a 35 msnm. Tiene forma cónica
              y una escalera de 154 escalones. Su <a
                href="http://es.wikipedia.org/wiki/Linterna"
                title="Linterna">linterna</a> alcanza más de 50 km, y es uno de
              los puntos de referencia marítima más antiguos de esta costa
              atlántica. De día, un barco puede observarlo hasta una distancia
              de 35 km.</li>

              <li><b>Usina 9 de Julio</b>: construida en 1950 por el ingeniero
              <a href="http://es.wikipedia.org/wiki/Jos%C3%A9_Zanier" >José
                Zanier</a> en la zona del puerto, dicha <a
                href="http://es.wikipedia.org/wiki/Central_termoel%C3%A9ctrica"
                >central termoeléctrica</a> fue precursora en su tipo y cuenta
              en su diseño con un oleoducto interconectado a la planta de YPF y
              túneles subterráneos unidos al mar para la refrigeración del
              agua. Todavía hoy sigue abasteciendo a la reconocida ciudad
              balnearia en una muestra cabal de aquel visionario
              emprendimiento.</li>
              <li><b><a
                  href="http://es.wikipedia.org/wiki/El_Galp%C3%B3n_de_las_Artes"
                  >El Galpón de las Artes</a></b>: es una sala de teatro
              independiente, creada en 1996, que produce obras de teatro, de
              carácter experimental, tanto para la infancia como para adultos y
              por la que han pasado más de 150 espectáculos independientes en
              sus primeros 10 años de existencia.</li>
            </ul>

            <h3>Para hacer... </h3>

            <p>Casino Central: uno de los principales símbolos de la ciudad. Su
            construcción fue proyectada en 1937 como parte de lo que se
            convertiría en la Rambla Casino. El casino es uno de los mayores
            del mundo, por su extensión, y aloja el Teatro Auditorium.</p>

            <p>Las muestras de Tango que se ofrecen en el Teatro Colón son
            fantásticas y por solo 15 pesos podrás disfrutar de las mejores
            compañías de danzas de Buenos Aires. </p>

            <p>Disfrutar el Mate, una infusión amarga estimulante común en la
            gastronomía local. Compra un mate, un termo, yerba y pregunta por
            agua caliente en casi cualquier lugar. Nadie deja su hogar sin
            llevarse consigo su mate. </p>

            <h3>Comprar... </h3>
            <p>El cambio actual favorece para compras de todo tipo.
            Por ejemplo, se puede comprar una chaqueta de cuero por
            cerca de U$S 40. Hoy en día (Agosto 2007), el cambio es de 3 pesos
            argentinos por dolar y de menos de un euro.</p>

            <h3>Comer... </h3>
            <p>Excelente pescado. El Puerto de Mar del Plata es una zona genial
            para conocer y probar mariscos, <a
              href="http://en.wikipedia.org/wiki/Fried_calamari">rabas</a> y
            tantas opciones como estés dispuesto a probar.</p>

            <p><span class="thumb"><a href="[% imgs %]/AlfajoresTriples.jpg"><img
                src="[% imgs %]/AlfajoresTriples.jpg" alt="Alfajores"
                title="Alfajores"/></a><br/>Alfajores</span>"Alfajores" (un
            postre tipo biscuit con <a
              href="http://en.wikipedia.org/wiki/Dulce_de_Leche">"dulce de
              leche"</a>) se encuentran en cualquier lugar de Argentina, y se
            llevan perfectamente con un <a
              href="http://en.wikipedia.org/wiki/Café_con_leche">"café con
              leche"</a>. Los fabricados por la marca "Havana" son de los mejores, y son vendidos en locales que se encuentran por toda la ciudad.
            </p>

            <p>Los Argentinos aman el <a
              href="http://en.wikipedia.org/wiki/Dulce_de_Leche">"dulce de
              leche"</a>, que es un dulce cremoso, y lo incluyen en
            casi cualquier producto dulce. Las Medialunas (mini-croissants) y
            el dulce de leche complementan un desayuno ideal. El helado de
            Dulce de leche granizado combina rayaduras de
            chocolate con dulce de leche en una crema helada.</p>

            <p><!--span class="thumb"><img src="images/bife_de_chorizo.jpg"
              alt="Una sabrosa porción de bife de chorizo" title="Una sabrosa
              porción de bife de chorizo"/><br/>Una sabrosa porción de bife de
              chorizo</span-->Las parrillas
            son siempre muy buenas elecciones; con el cambio
            actual, comer parrilla todos los días es muy posible.
            Uno de los mejores platos es el "bife de chorizo", un corte de
            ternera que se halla pegada al costillar del animal, no confundir
            con el embutido que lleva el <a
              href="http://es.wikipedia.org/wiki/Chorizo">mismo nombre</a>.</p>

            <p>Las empanadas en Argentina son otra típica comida local, constan
            de un relleno envuelto en una masa fina a base de harina. Los
            rellenos más típicos son carne, jamón y queso y otros, pero se
            consiguen rellenas de casi cualquier cosa, incluyendo algunas
            dulces. Ideales hack-not-so-snacks ;)</p>

            <p>Luego de haber entrado en un coma a causa de la ingestión de
            tanta carne, existen muchos <a
              href="http://en.wikipedia.org/wiki/Buffet">"Tenedores Libres"</a>
            orientales y muchísimas variedades de mariscos en la zona aledaña
            al "Puerto".</p>

            <h3>De copas... </h3>

            <p>La calle Alem es el centro de la vida nocturna de la ciudad.
            Se pueden encontrar una incontable cantidad de Pubs, Bares, y
            clubes nocturnos en aproximadamente 6 manzanas a la redonda.</p>

            <p>Las discos están en la zona de la avenida Constitución, por 30
            pesos, <a href="http://www.sobremonte.com.ar/">"Sobremonte"</a> es
            una gran experiencia. "Chocolate" y "Gap" son también muy buenas
            opciones.</p>

            <p>En la calle Güemes, en una atmósfera más relajada, hay café
            concerts con música en vivo.</p>

            <h3>Fuentes:</h3>
            <ul>
              <li><a href="http://wikitravel.org/en/Mar_del_Plata"
                >WikiTravel.org</a></li>
              <li><a href="http://es.wikipedia.org/wiki/Mar_Del_Plata"
                >Wikipedia.org</a> </li>
              <li><a
                href="http://www.turismomardelplata.gov.ar/contenido/default.asp"
                >Ente municipal de turismo, (EMTUR - Mar del Plata)</a></li>
            </ul>
