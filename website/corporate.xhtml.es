[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Asistentes corporativos y profesionales"
  lang = "es"
%]
            <h2>Asistentes corporativos participando de DebConf8</h2>

            <table style="width: 90%" class="fancy-table">
              <col />
              <col class="logos" />
              <tbody>
                <tr class="rowA">
                  <td>Bdale Garbee<br/>HP</td>
                  <td><a href="http://www.hp.com/"><img src="[% splogos %]/platinum/HP.png" /></a></td>
                </tr>
                <tr class="rowB">
                  <td>Lionel Elie Mamane<br/>Gestman s.a., Luxembourg</td>
                  <td><img src="[% splogos %]/corporate/gestman.png" /></td>
                </tr>
                <tr class="rowA">
                  <td>Torben Fjerdingstad<br/>UNI-C</td>
                  <td><a href="http://www.uni-c.dk/"><img src="[% splogos %]/corporate/uni-c2.png" /></a></td>
                </tr>
                <tr class="rowB">
                  <td>Torsten Werner<br />German Federal Foreign Office</td>
                  <td><a href="http://www.auswaertiges-amt.de/"><img src="[% splogos %]/corporate/gffo.png" /></a></td>
                </tr>
              </tbody>
            </table>

            <h2>Asistentes profesionales participando de DebConf8</h2>

            <table style="width: 90%" class="fancy-table">
              <tbody>
                <tr class="rowA">
                  <td>Asheesh Laroia </td>
                  <td><a href="http://creativecommons.org/">Creative Commons</a></td>
                </tr>
                <tr class="rowB">
                  <td>Barton George </td>
                  <td><a href="http://www.sun.com/">Sun Microsystems</a></td>
                </tr>
                <tr class="rowA">
                  <td>Carlos Briosso </td>
                  <td><a href="http://www.gia-sa.com">General Industries Argentina S.A.</a></td>
                </tr>
                <tr class="rowB">
                  <td>Diego J. Brengi </td>
                  <td><a href="http://www.inti.gov.ar/">INTI – Instituto Nacional de Tecnología Industrial</a></td>
                </tr>
                <tr class="rowA">
                  <td>Eric Evans </td>
                  <td><a href="http://www.rackspace.com/">Rackspace Hosting</a></td>
                </tr>
                <tr class="rowB">
                  <td>Erinn Clark </td>
                  <td><a href="http://scan.coverity.com">Coverity</a></td>
                </tr>
                <tr class="rowA">
                  <td>Fernando Ike de Oliveira </td>
                  <td><a href="http://www.b2br.com.br">B2BR</a></td>
                </tr>
                <tr class="rowB">
                  <td>Frédéric Daniel Luc Lehobey </td>
                  <td><a href="http://proxience.com">Proxience</a></td>
                </tr>
                <tr class="rowA">
                  <td>Henry Rivero</td>
                  <td><a href="http://www.cnti.gob.ve">Centro Nacional de Tecnologías de Información</a></td>
                </tr>
                <tr class="rowB">
                  <td>Jacob Appelbaum </td>
                  <td><a href="https://www.torproject.org/">Tor Project</a></td>
                </tr>
                <tr class="rowA">
                  <td>Jean-René Mérou Sánchez </td>
                  <td><a href="http://www.hispalinux.es/">Hispalinux</a></td>
                </tr>
                <tr class="rowB">
                  <td>Jono Bacon </td>
                  <td><a href="http://www.canonical.com/">Canonical</a></td>
                </tr>
                <tr class="rowA">
                  <td>Jorge O. Castro </td>
                  <td><a href="http://www.canonical.com/">Canonical</a></td>
                </tr>
                <tr class="rowB">
                  <td>José Rafael Alvarado Mendoza </td>
                  <td><a href="http://www.cnti.gob.ve">Centro Nacional de Tecnologías de Información</a></td>
                </tr>
                <tr class="rowA">
                  <td>Jurij Smakov </td>
                  <td><a href="http://www.google.com">Google Inc.</a></td>
                </tr>
                <tr class="rowB">
                  <td>Kees Cook </td>
                  <td><a href="http://www.canonical.com/">Canonical</a></td>
                </tr>
                <tr class="rowA">
                  <td>Mark Shuttleworth </td>
                  <td><a href="http://www.canonical.com/">Canonical</a></td>
                </tr>
                <tr class="rowB">
                  <td>Matthias Klose </td>
                  <td><a href="http://www.canonical.com/">Canonical</a></td>
                </tr>
                <tr class="rowA">
                  <td>Michael 'grisu' Bramer </td>
                  <td><a href="http://www.deb-support.de/">deb-support.de</a></td>
                </tr>
                <tr class="rowB">
                  <td>Michael Shuler </td>
                  <td><a href="http://www.rackspace.com/">Rackspace Hosting</a></td>
                </tr>
                <tr class="rowA">
                  <td>Ralf Treinen </td>
                  <td><a href="http://www.mancoosi.org">The Mancoosi project</a></td>
                </tr>
                <tr class="rowB">
                  <td>Robert Jaehne </td>
                  <td><a href="http://www.muenchen.de">City of Munich</a></td>
                </tr>
                <tr class="rowA">
                  <td>Salvador Eduardo Tropea</td>
                  <td><a href="http://www.inti.gov.ar/">INTI – Instituto Nacional de Tecnología Industrial</a></td>
                </tr>
                <tr class="rowB">
                  <td>Stefano Zacchiroli </td>
                  <td><a href="http://www.mancoosi.org">The Mancoosi project</a></td>
                </tr>
                <tr class="rowA">
                  <td>Steve Langasek </td>
                  <td><a href="http://www.canonical.com/">Canonical</a></td>
                </tr>
                <tr class="rowB">
                  <td>Sulamita Garcia </td>
                  <td><a href="http://www.intel.com/">Intel</a></td>
                </tr>
                <tr class="rowA">
                  <td>Wouter Verhelst </td>
                  <td><a href="http://www.nixsys.be/">NixSys BVBA</a></td>
                </tr>
              </tbody>
            </table>

            <h2>Otros asistentes profesionales que también participan de DebConf8</h2>

            <table style="width: 90%" class="fancy-table">
              <tbody>
                <tr class="rowA">
                  <td>Aki Uekawa </td>
                  <td>Florian Maier </td>
                  <td>Manoj Srivastava </td>
                </tr>
                <tr class="rowB">
                  <td>Ana María Espósito </td>
                  <td>Gonzalo Ezequiel Bermúdez </td>
                  <td>Marco Túlio Gontijo e Silva </td>
                </tr>
                <tr class="rowA">
                  <td>Andree Yacoy Carvajal </td>
                  <td>Jacobo Tarrío </td>
                  <td>María Lourdes Racca </td>
                </tr>
                <tr class="rowB">
                  <td>Andrew Saunders </td>
                  <td>James Vega </td>
                  <td>Matthias Schmitz </td>
                </tr>
                <tr class="rowA">
                  <td>Anne Goldenberg </td>
                  <td>John Wright </td>
                  <td>Mauro A. Meloni </td>
                </tr>
                <tr class="rowB">
                  <td>Bruce Leidl </td>
                  <td>Jose Antonio Recio Cuesta </td>
                  <td>Nelson Marquina </td>
                </tr>
                <tr class="rowA">
                  <td>Carmen Cordero </td>
                  <td>Juan de La Cruz Navas Gimenez </td>
                  <td>Rafael Ernesto Rivas Méndez </td>
                </tr>
                <tr class="rowB">
                  <td>Christoph Fink </td>
                  <td>Juan M. Rodriguez M. </td>
                  <td>Sébastien Grenier </td>
                </tr>
                <tr class="rowA">
                  <td>Edward Ortega </td>
                  <td>Luciana Fujii </td>
                  <td>Viviana Garcia Machorro </td>
                </tr>
              </tbody>
            </table>
