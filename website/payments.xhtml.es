[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Pagos y Donaciones para DebConf8"
  lang = "es"
%]
            <h2>Pagos y Donaciones para DebConf8</h2>
            <p><a href="http://www.spi-inc.org/">Software in the Public
              Interest</a> está colaborando con DebConf8 mediante la recepción
            de los pagos referidos a los costos de asistencia o de donaciones
            para DebConf8. Esto se realiza mediante una página provista por <a
              href="http://www.clickandpledge.com/">Click&amp;Pledge</a>, que
            factura el costo directamente a una tarjeta de crédito.</p>

            <p>Para hacer el pago o donación, vaya a <a
              href="https://co.clickandpledge.com/advanced/default.aspx?wid=20516"
              >esta página</a> e ingrese el monto a transferir.</p>

            <p>Otra opción, para gente en Argentina, es transferencia por CBU o
            depósitos a una cuenta de <a href="http://www.vialibre.org.ar/">Via
            Libre</a> que también está colaborando como receptora de estos
            pagos en Argentina.</p>

            <p>Los datos de la cuenta son:<br /><br />
            Cuenta Corriente en Pesos Nº: 100-224814<br />
            Banco:     Credicoop<br />
            Titular:   Fundación para la Difusión del Conocimiento y el Desarrollo Sustentable Vía Libre<br />
            CBU:       1910100455010002248146<br />
            CUIT:      30-70747162-6
            </p>

            <p>Con respecto a los montos, el costo para asistentes Corporativos
            es de USD 1000 por semana, para asistentes Profesionales es de USD
            300 por semana, pero para aquellos se hayan registrado antes del 31
            de mayo tiene un costo reducido de USD 250, aunque aún así pueden
            optar por pagar los USD 300 para colaborar con la organización de
            DebConf8.</p>

            <p>Quienes quieran tener una habitación individual, deberán abonar
            USD 250 adicionales por semana, tanto si son Independientes como si
            son Profesionales, mientras que los asistentes Corporativos no
            requieren pago extra.</p>

            <p>Por último, a quienes piensen asistir al DebCamp como
            Independientes sin un plan específico de trabajo les solicitamos
            que costeen su alojamiento y estadía pagando USD 250 por esa
            semana.</p>

            <p>Ante cualquier duda sobre cuánto pagar, por favor envíe un email
            a <a href="mailto:rooms@debconf.org">rooms@debconf.org</a>.</p>
