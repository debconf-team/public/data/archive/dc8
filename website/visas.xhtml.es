[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Visas"
  lang = "es"
%]
            <h2>Información sobre visas</h2>
            <p>Los paises que necesitan visa para entrar a Argentina, al dia 30 de Julio 2007 son:</p>
            <ul>
            <li>Afganistán</li>
            <li>Albania</li>
            <li>Angola</li>
            <li>Antigua y Barbuda</li>
            <li>Arabia Saudita</li>
            <li>Argelia</li>
            <li>Armenia</li>
            <li>Azerbaiján</li>
            <li>Bahamas</li>
            <li>Bahrein</li>
            <li>Bangladesh</li>
            <li>Belice</li>
            <li>Benin</li>
            <li>Bhutan</li>
            <li>Bielorrusia</li>
            <li>Bosnia y Herzegovina</li>
            <li>Botswana</li>
            <li>Brunei</li>
            <li>Bulgaria</li>
            <li>Burkina Faso</li>
            <li>Burundi</li>
            <li>Cabo Verde</li>
            <li>Camboya</li>
            <li>Camerún</li>
            <li>Chad</li>
            <li>China</li>
            <li>Comores</li>
            <li>Congo</li>
            <li>Corea</li>
            <li>Costa de marfil</li>
            <li>Cuba</li>
            <li>Dijibouti</li>
            <li>Dominica</li>
            <li>Egipto</li>
            <li>Emiratos Arabes Unidos</li>
            <li>Eritrea</li>
            <li>Estonia</li>
            <li>Etiopía</li>
            <li>Federación Rusa</li>
            <li>Fidji</li>
            <li>Filipinas</li>
            <li>Gabón</li>
            <li>Gambia</li>
            <li>Georgia</li>
            <li>Ghana</li>
            <li>Grenada</li>
            <li>Guinea</li>
            <li>Guinea Bissau</li>
            <li>Guinea Ecuatorial</li>
            <li>Guyana</li>
            <li>India</li>
            <li>Indonesia</li>
            <li>Irak</li>
            <li>Irán</li>
            <li>Isla Cabo Verde</li>
            <li>Islas Comores</li>
            <li>Jordania</li>
            <li>Kazajstán</li>
            <li>Kenya</li>
            <li>Kirguistán</li>
            <li>Kuwait</li>
            <li>Laos</li>
            <li>Lesotho</li>
            <li>Letonia</li>
            <li>Líbano</li>
            <li>Liberia</li>
            <li>Libia</li>
            <li>Macedonia</li>
            <li>Madagascar</li>
            <li>Malawi</li>
            <li>Maldivas</li>
            <li>Mali</li>
            <li>Marruecos</li>
            <li>Mauricio</li>
            <li>Mauritania</li>
            <li>Micronesia</li>
            <li>Moldova</li>
            <li>Mongolia</li>
            <li>Mozambique</li>
            <li>Myanmar</li>
            <li>Namibia</li>
            <li>Nepal</li>
            <li>Niger</li>
            <li>Nigeria</li>
            <li>Omán</li>
            <li>Pakistán</li>
            <li>Papua Nueva Guinea</li>
            <li>Qatar</li>
            <li>República Centroafricana</li>
            <li>Rwanda</li>
            <li>Samoa</li>
            <li>San Cristóbal y Nevis</li>
            <li>San Vicente y las Granadinas</li>
            <li>Santo Tomé y Príncipe</li>
            <li>Senegal</li>
            <li>Seychelles</li>
            <li>Sierra Leona</li>
            <li>Siria</li>
            <li>Somalía</li>
            <li>Sri Lanka</li>
            <li>Sudán</li>
            <li>Surinam</li>
            <li>Swazilandia</li>
            <li>Tailandia</li>
            <li>Túnez</li>
            <li>Turkmenistán</li>
            <li>Ucrania</li>
            <li>Uganda</li>
            <li>Uzbekistán</li>
            <li>Vanuatu</li>
            <li>Vietnam</li>
            <li>Yemen</li>
            <li>Zambia</li>
            <li>Zimbabwe</li>
          </ul>
          <p>Fuente Oficial: <a href="http://www.cancilleria.gov.ar/portal/temas_consulares/visas.html">sitio web de la cancillería argentina</a></p>
