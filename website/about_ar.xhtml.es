[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Sobre Argentina"
  lang = "es"
%]
            <h2>Sobre Argentina</h2>
            <p>Esta es una breve introducci&oacute;n a la Argentina y su
            cultura para que sirva de gu&iacute;a a los extranjeros que
            planeen asistir a DebConf8 en Mar del Plata.
            Pod&eacute;s ayudar enviando tus dudas sobre la Argentina a <a
              href="mailto:debconf8-localteam@lists.debconf.org"
              >debconf8-localteam@lists.debconf.org</a>,
            ser&aacute;n contestadas por correo electr&oacute;nico y la
            informaci&oacute;n recolectada agregada a esta p&aacute;gina.</p>

            <h3>Geograf&iacute;a</h3>
            <p>Argentina tiene una superficie aproximada de 2.8 millones de 
            Km2, y una poblaci&oacute;n que alcanza casi los 39 millones de
            habitantes. Abarca una amplia variedad de climas desde el 
            subtropical en la regi&oacute;n norte hasta el subpolar en el
            extremo sur, aunque el predominante es el templado.</p>

            <div>
            <img
            src="[% imgs %]/LocationArgentina.png"
            alt='Mapa America Latina' />
            <img
            src="[% imgs %]/LocationArgentinaContinent.png"
            alt='Mapa America Latina' />
            </div>

            <h3>Moneda y Econom&iacute;a</h3>
            <p>La moneda Argentina es el Peso, cuyo s&iacute;mbolo es $, el cambio oficial
            es publicado a diario en los peri&oacute;dicos, o puede consultarse en el
            <a href="http://www.bna.com.ar/bp/bp_cotizaciones.asp?op=m">Banco Nacion</a>.</p>
            
            <p>Los billetes en circulaci&oacute;n son: $2, $5, $10, $20, $50 y
            $100; y las monedas: $0.05, $0.10, $0.25, $0.50 y $1.</p>

            <p>Para más información y fotos de billetes y monedas circulantes,
            ver <a href="http://en.wikipedia.org/wiki/Argentine_peso">la página
              de Wikipedia para el peso argentino (en inglés)</a>.</p>

            <h3>Electricidad</h3>
            <p><span class="thumb">
                <img src="[% imgs %]/Australian_dual_switched_power_point.jpg"
                alt="Type I socket" title="Type I socket image taken from
                Wikipedia"/>
              <br/>Enchufe tipo I</span>

			Argentina usa 220 Volts a 50 Hercios. El enchufe estandard es el
			enchufe tipo I, también conocido como <a
			href="http://en.wikipedia.org/wiki/AS_3112">AS 3112</a>, usado
			también en Nueva Zelanda, Australia y algunos otros paises. En
			Argentina esta listado como IRAM 2073, sin embargo las diferencias
			son despreciables.
            
			Algunos aparatos que tiene doble aislacion vienen sin la para para
			conexión a tierra.</p>

            <p><span class="thumb">
                <img src="[% imgs %]/Europlug2.jpg"
                alt="Type C plug" title="Type C plug image taken from
                Wikipedia"/>
              <br/>Enchufe tipo C</span>
            Todavia se usa mucho el enchufe anterior tipo C, también conocido como <a
              href="http://en.wikipedia.org/wiki/Europlug">enchufe europeo</a>,

			sin embargo no está permitido de usar en nuevos artefactos, ni
			nuevas instalaciones. Es muy común encontrar lugares
			donde tienen este tipo de tomas corriente, y tomas que son
			compatibles tanto con este tipo como el estandar actual.</p>
            
			<p>Adaptadores para uno y otro enchufe son fáciles de encontrar,
			también para permitir conectar enchufes tipo A (US sin conexión a
			tierra). Las zapatillas con conexiones multiples (tipos A,
			C, I y más difícilmente E, F o L) se pueden conseguir
			en cualquier ferretería. No esperen encontrar conexión a
			tierra en instalaciones viejas.
            
            Otros adaptadores son muy difíciles de encontrar.</p>

            <h3>M&aacute;s informaci&oacute;n</h3>
            <ul>
            <li> <a href="http://www.argentina.gov.ar">www.argentina.gov.ar</a> </li>
            <li> <a href="http://es.wikipedia.org/wiki/Argentina">Wikipedia</a> </li>
            </ul>

            <h3>Devolución del IVA</h3>
            <p>La oficina local de turismo ofrece información para turistas
            extranjeros que deseén recuperar el IVA de sus compras. Para
            conocer sobre este beneficio, dirígase al <a
              href="http://www.turismomardelplata.gov.ar/contenido/DevolucionIvaExtranjeros_Mar_Del_Plata.asp"
              >sitio web</a>.</p>

