[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Guía para el aeropuerto de Ezeiza"
  lang = "en"
%]
            <h2>Guía para el aeropuerto de Ezeiza</h2>
            <p>El <a
              href="http://es.wikipedia.org/wiki/Aeropuerto_Internacional_Ministro_Pistarini"
              >aeropuerto internacional de Buenos Aires</a> se llama “Ministro
            Pistarini”, y su código internacional es 
            “EZE” por Ezeiza, nombre de la zona donde está ubicado.</p>

            <p>Los vuelos de todas las líneas, excepto Aerolíneas Argentinas 
			y Austral, llegan a y salen de la <strong>terminal A</strong>.</p>

            <p><span class="thumb"><a href="[% imgs %]/ezeiza_01.jpg"><img
                src="[% imgs %]/ezeiza_t01.jpg" alt="Área de arribos"
                title="Arrival Zone" /></a><br />Área de arribos</span>
				
			Una vez pasadas las migraciones, retiro de equipaje y aduana,
			se llega al hall del área de arribos pasando por al
			lado de locales de diferentes empresas.  El primero a la
			izquierda es “<a href="http://www.tiendaleonmdq.com/">Manuel
			Tienda León</a>”, la compañía de buses que va a Mar del
			Plata.</p>

			<p><span class="thumb"><a href="[% imgs %]/ezeiza_02.jpg"><img
			src="[% imgs %]/ezeiza_t02.jpg" alt="Oficina de Manuel Tienda
			León" title="Oficina de Manuel Tienda León" /></a><br />Oficina
			de Manuel Tienda León </span>Puedes hablar con ellos allí
			mismo, o podés salir del área de arribos hacia la izquierda y
			hablar con ellos desde fuera.</p>

			<p><span class="thumb"><a href="[% imgs %]/ezeiza_03.jpg"><img
			src="[% imgs %]/ezeiza_t03.jpg" alt="Frente de la Oficina de
			Manuel Tienda León" title="Frente de la Oficina de Manuel
			Tienda León" /></a><br />Frente de la Oficina de Manuel Tienda
			León</span> En cualquier caso, conviene que tengas tu reserva
			impresa y se las muestres, de manera que ellos puedan confirmar
			tu presencia en el ómnibus.</p>

			<p>Más tarde, deberás presentarte en la ventanilla de la
			empresa con 10 minutos de antelación a la partida del ómnibus;
			harán un llamado por lista y acompañarán a todos los que tomen
			el ómnibus hasta donde está estacionado.  Mientras tanto,
			podrás cambiar dinero, tomar un café o hacer lo que prefieras,
			no es necesario que esperes allí.</p>

			<h3 style="clear: both">Cambiar dinero en el aeropuerto</h3>
			<p><span class="thumb"><a href="[% imgs %]/ezeiza_04.jpg"><img
			src="[% imgs %]/ezeiza_t04.jpg" alt="Lugares de cambio"
			title="Exchange places" /></a><br />Lugares de cambio</span> Al
			llegar al aeropuerto, verás en seguida un puesto de "Global
			Exchage". <strong>NO</strong> cambies dinero con ellos.  Tienen
			<strong>muy muy</strong> malas tasas de cambio.  Hay otros
			lugares donde se puede cambiar dinero a tasas aceptables.</p>
				
            <p><span class="thumb"><a href="[% imgs %]/ezeiza_05.jpg"><img
                src="[% imgs %]/ezeiza_t05.jpg" alt="Banco Nacion" title="Banco
                Nacion" /></a><br />Banco Nacion</span>A la derecha del hall de
				arribos encontrarás una sucursal del "Banco Nación" con un
				cajero automático.</p>

            <p><span class="thumb"><a href="[% imgs %]/ezeiza_06.jpg"><img
                src="[% imgs %]/ezeiza_t06.jpg" alt="Banco Piano zona de check-in"
                title="Banco Piano zona de check-in" /></a><br />Banco Piano
              zona de check-in</span>Es posible que la cola en el "Banco Nación" sea medianamente larga.  Si es así y tenés apuro, podés ir a uno de los dos locales del "Banco Piano"</p>

				<p>El primero está ubicado a la derecha de la zona de
				arribos, en la zona de check-in, frente a los mostradores
				64-66, debajo de las escaleras eléctricas.</p>

			<p style="clear: both"><span class="thumb"><a href="[% imgs
			%]/ezeiza_07.jpg"><img src="[% imgs %]/ezeiza_t07.jpg"
			alt="Ubicación del Banco Piano en el primer piso"
			title="Ubicación del Banco Piano en el primer piso" /></a><br
			/>Ubicación del Banco Piano en el primer piso</span> El segundo
			está ubicado en el primer piso, junto a las puertas de
			embarque, junto a varios otros negocios.</p>

            <p><span class="thumb"><a href="[% imgs %]/ezeiza_08.jpg"><img
                src="[% imgs %]/ezeiza_t08.jpg" alt="Banco Piano del primer piso"
                title="Banco Piano del primer piso" /></a><br />Banco Piano del primer piso</span>Ambos tienen el mismo tipo de cambio.</p>

			<p><span class="thumb"><a href="[% imgs %]/ezeiza_09.jpg"><img
			src="[% imgs %]/ezeiza_t09.jpg" alt="Cajeros automáticos"
			title="Cajeros automáticos" /></a><br />Cajeros
			automáticos</span>Si podés obtener dinero directamente de
			cajeros automáticos, podés utilizar los que se encuentran junto
			a las puertas de salida del aeropuerto.  Te recomendamos que
			verifiques con tu banco cuál será el costo de retirar dinero
			mediante estos cajeron en Argentina, ya que puede tratarse de
			una comisión grande o pequeña según el banco, el tipo de
			cuenta, etc.</p>

            <h3 style="clear: both">Llegadas a la Terminal B</h3>

			<p>Si por algún motivo llegás al aeropuerto a
			través de la Terminal B, podés contactar allí mismo a los
			representantes de "Manuel Tienda León" y también cambiar
			dinero, pero tendrás que ir hacia la Terminal A para tomar el
			ómnibus.</p>

			<p><span class="thumb"><a href="[% imgs %]/ezeiza_10.jpg"><img
			src="[% imgs %]/ezeiza_t10.jpg" alt="Oficina de Manuel Tienda
			León - Terminal B" title="Oficina de Manuel Tienda León -
			Terminal B" /></a><br />Oficina de Manuel Tienda León -
			Terminal B</span>El local de "Manuel Tienda León" se encuentra
			justo a la salida de la aduana, a la derecha</p>
			
			<p><span class="thumb"><a href="[% imgs %]/ezeiza_11.jpg"><img
			src="[% imgs %]/ezeiza_t11.jpg" alt="Banco Nacion - Terminal B"
			title="Banco Nacion - Terminal B" /></a><br />Banco Nacion -
			Terminal B</span>La sucursal del “Banco Nación” se encuentra
			detrás del pasillo de salida. Como antes, si la cola en este
			local es muy larga, es posible ir a la otra Terminal.  La
			distancia entre ambas es de unos 2 minutos a pie.</p>
