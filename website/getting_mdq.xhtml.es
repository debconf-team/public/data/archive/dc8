[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Llegar a Mar del Plata"
  lang = "es"
%]
            <h2>Llegar a Mar del Plata</h2>
            <p>¡Asegurate de también mirar la <a
              href="http://wiki.debconf.org/wiki/Argentina/Travel">página
              wiki</a> sobre este tema!.</p>
            <p><strong>Nuevo</strong>: <a href="ezeiza.xhtml[% langext %]"
              >[% t.leftmenu.ezeiza %]</a></p>

            <h3>En avión</h3>

            <p>
            Si estás llegando a Argentina en avión vas a
            estar aterrizando en el <a
            href="http://es.wikipedia.org/wiki/Aeropuerto_Internacional_Ministro_Pistarini">aeropuerto de Ezeiza</a>, 
            pero los vuelos a Mar del Plata salen únicamente del <a
            href="http://es.wikipedia.org/wiki/Aeroparque_Jorge_Newbery">Aeroparque Jorge Newbery</a>.
            <a href="http://www.tiendaleon.com.ar">Manuel Tienda León</a> tiene
            buses Ezeiza-Aeroparque  cada hora a partir de las 9:00 y hasta
            las 00:40, tanto de ida como de vuelta. La duración del viaje
            es de una hora y cuesta alrededor de 14€.
            </p>

            <p>
            Aerolíneas Argentinas tiene dos vuelos diarios en ambos sentidos
            los días de semana y sólo uno los fines de  semana. Los
            horarios de salida desde Buenos Aires son aproximadamente a las
            7:00 y a las 20:00, y desde Mar del Plata a las 9:00 y a las 21:00.
            El costo es de 74€ en clase turista y la duración de
            una hora.
            </p>

            <p>Información gubernamental acerca de <a
              href="http://www.argentina.gov.ar/argentina/portal/paginas.dhtml?pagina=140"
              >viajar a Argentina</a>.</p>

            <h3>En bus</h3>
            <p>Si querés arreglar con otra gente para tomar el bus, completá
            tus datos en la <a
              href="http://wiki.debconf.org/wiki/DebConf8/EZE-MDQ_bus">página
              wiki </a>.</p>
            <p>
            Hay 8 viajes diarios regulares desde Ezeiza hasta Mar del Plata
            ofrecidos por el servicio de <a href="http://www.tiendaleon.com.ar">
            Manuel Tienda León</a> por cerca de 37€ ida y vuelta con una 
            duración de 6 horas.
            </p>

            <p>Los horarios de salida Buenos Aires-MDQ y MDQ-Buenos Aires
            son:</p>
            <table>
            <tr> 
              <td>MDQ</td><td>Ezeiza</td> 
              <td>&nbsp;</td>
              <td>Ezeiza</td><td>MDQ</td>
            </tr>
            <tr>
              <td>01:00</td>  <td>07:00</td> 
              <td>&nbsp;</td>
              <td>02:30</td>  <td>08:20</td>
            </tr>
            <tr>
              <td>04:00</td>  <td>09:40</td> 
              <td>&nbsp;</td>
              <td>05:30</td>  <td>11:10</td>
            </tr>
            <tr> 
              <td>07:00</td>  <td>13:00</td> 
              <td>&nbsp;</td>
              <td>10:00</td>  <td>15:40</td>
            </tr>
            <tr>
              <td>12:00</td>  <td>18:00</td> 
              <td>&nbsp;</td>
              <td>12:30</td>  <td>18:20</td>
            </tr>
            <tr>
              <td>17:00</td>  <td>22:40</td> 
              <td>&nbsp;</td>
              <td>14:30</td>  <td>20:20</td>
            </tr>
            <tr>
              <td>22:00</td>  <td>04:00</td> 
              <td>&nbsp;</td>
              <td>21:30</td>  <td>03:20</td>
            </tr>
            </table>


            <p>Es posible reservar enviando un e-mail a
            reservas@tiendaleonmdq.com.</p>

            <p>Fuente: <a href="http://www.tiendaleonmdq.com/">Tienda León
              MDQ</a></p>
