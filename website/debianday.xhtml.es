[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "DebianDay"
  lang = "es"
%]
      <h2>DebianDay</h2>

      <p>El 18 de Agosto será el DebianDay, una conferencia corta
      apuntada a usuarios de Debian e interesados en el software
      libre.  Se realizará en Sarmiento 1867 ("Círculo de Oficiales
      de Mar"), Ciudad de Buenos Aires  Argentina, a partir de las
      10hs.  </p>

      <p>Este año tendremos dos tracks de charlas uno destinado a quienes
      recién entran al mundo del software libre y otro para quienes les
      interesan temas más avanzados.
      </p>

      <h2>Cronograma</h2>

      <p>Cronograma tentativo de las charlas programadas.
      </p>

      <table class="fancy-table">
        <col class="date" />
        <col class="description" />
        <col class="description" />
        <thead>
          <tr class="rowH">
            <th>Horario</th>
            <th>Salón "etch"</th>
            <th>Salón "lenny"</th>
          </tr>
        </thead>
        <tbody>
          <tr class="rowA">
            <td><strong>9:45</strong></td>
            <td colspan="2" align="center"><strong>Apertura del
                Evento</strong></td>
          </tr>
          <tr class="rowB">
            <td><strong>10:00</strong>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/309.en.html"><strong>¿Qué es el Software Libre?<br/></strong> Gunnar Wolf</a>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/298.en.html"><strong>Herding Wild Cats (An Inside Look at the Debian Project)<br/></strong> Bdale Garbee (charla en ingles)</a>
            </td>
          </tr>
          <tr class="rowA">
            <td><strong>11:00</strong>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/315.en.html"><strong>BoliviaOS<br/></strong> Dydier Rojas Guerrero</a>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/248.en.html"><strong>Knowledge, Power and free Beer<br/></strong> Andreas Tille (charla en ingles)</a>
            </td>
          </tr>
          <tr class="rowB">
            <td><strong>12:00</strong>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/314.en.html"><strong>Casos de éxito en implementaciones empresariales<br/></strong> José Miguel Parrella Romero</a>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/321.en.html"><strong>Debian Secrets<br/></strong> Wouter Verhelst (charla en ingles)</a>
            </td>
          </tr>
          <tr class="rowA">
            <td><strong>13:00</strong>
              </td><td colspan="2" align="center"><b>Almuerzo</b>
            </td>
          </tr>
          <tr class="rowB">
            <td><strong>15:00</strong>
              </td><td><strong>Software como medio de expresión<br/></strong> Federico Heinz
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/312.en.html"><strong>Proyecto gnuLinEx<br/></strong> César Gómez Martín</a>
            </td>
          </tr>
          <tr class="rowA">
            <td><strong>16:00</strong>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/344.en.html"><strong> "Todas las distribuciones de GNU/Linux son seguras"... pero algunas son más seguras que otras.<br/></strong> Pedro O. Varangot (Core Security Technologies)</a>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/341.en.html"><strong>Linux Around the World<br/></strong> John "maddog" Hall (charla en ingles)</a>
            </td>
          </tr>
          <tr class="rowB">
            <td><strong>17:00</strong>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/311.en.html"><strong>Internacionalización en Debian<br/></strong> César Gómez Martín</a>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/288.en.html"><strong>The debian videoteam - behind the scenes<br/></strong> Holger Levsen (charla en ingles)</a>
            </td>
          </tr>
          <tr class="rowA">
            <td><strong>18:00</strong>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/323.en.html"><strong>Hosting Caseros: armá tu propio webserver<br/></strong> Sebastián Montini</a>
              </td><td><a href="https://penta.debconf.org/dc8_schedule/events/360.en.html">
			  <strong>Empaquetar software para Debian<br/></strong> Maximiliano Curia</a>
            </td>
          </tr>
          <tr class="rowB">
            <td><strong>19:00</strong>
              </td><td><strong>Cómo colaborar con Debian: manejo de bugs,
			  paquetes, traducciones y demás<br/></strong> Margarita Manterola
            </td>
          </tr>
          <tr class="rowA">
            <td><strong>20:00</strong>
              </td><td colspan="2" align="center" ><b>Cierre del evento</b>
            </td>
          </tr>
        </tbody>
      </table>
      <br/>
      <br/>

      <h2>Auspiciantes</h2>

      <p>Desde ya les agradecemos a todos los auspiciantes que apoyan
      este evento y el software libre en general, sin ellos nada de esto
      podria ser posible.</p>

      <div class="sponsors" style="margin: 0 3em;">
        <a href='http://www.coresecurity.com/' title='Core'
          ><img style="height: 55px; float: left"
          src="http://media.debconf.org/dc8/sponsorlogos/bronze/core.png"
          alt='Core' /></a> 
        <a href='http://www.vialibre.org.ar/' title='Fundación Vía Libre'
          ><img style="float: right"
          src="http://media.debconf.org/dc8/sponsorlogos/support/ViaLibre.png"
          alt='Fundación Vía Libre' /></a></div>




